package com.pathashala;

class Rover {
    private int xCoordinate;
    private int yCoordinate;
    private char faceTowards;

    Rover(int xCoordinate, int yCoordinate, char faceTowards) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.faceTowards = faceTowards;
    }

    void turn(char command) {
        if (command == 'L') {
            switch (faceTowards) {
                case 'N':
                    faceTowards = 'W';
                    break;
                case 'W':
                    faceTowards = 'S';
                    break;
                case 'S':
                    faceTowards = 'E';
                    break;
                case 'E':
                    faceTowards = 'N';
                    break;
            }
        } else if (command == 'R') {
            switch (faceTowards) {
                case 'N':
                    faceTowards = 'E';
                    break;
                case 'E':
                    faceTowards = 'S';
                    break;
                case 'S':
                    faceTowards = 'W';
                    break;
                case 'W':
                    faceTowards = 'N';
                    break;
            }
        }
    }

    Rover move(Plateau plateau) {
        if (faceTowards == 'N') {
            if (plateau.isPositionExisting(xCoordinate, yCoordinate + 1))
                yCoordinate += 1;
        } else if (faceTowards == 'S') {
            if (plateau.isPositionExisting(xCoordinate, yCoordinate - 1))
                yCoordinate -= 1;
        } else if (faceTowards == 'W') {
            if (plateau.isPositionExisting(xCoordinate - 1, yCoordinate))
                xCoordinate -= 1;
        } else if (faceTowards == 'E') {
            if (plateau.isPositionExisting(xCoordinate + 1, yCoordinate))
                xCoordinate += 1;
        }
        return this;
    }

    char getFace() {
        return faceTowards;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Rover) {
            Rover otherRover = (Rover) object;
            return this.faceTowards == otherRover.faceTowards &&
                    this.yCoordinate == otherRover.yCoordinate &&
                    this.xCoordinate == otherRover.xCoordinate;
        }
        return false;
    }

    public Rover execute(Plateau plateau, String commandsForFirstRover) {
        int iterator;
        for (iterator = 0; iterator < commandsForFirstRover.length(); iterator++) {
            if (commandsForFirstRover.charAt(iterator) == 'M') {
                this.move(plateau);
            } else {
                this.turn(commandsForFirstRover.charAt(iterator));
            }
        }
        return this;
    }
}
