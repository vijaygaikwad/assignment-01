package com.pathashala;

class Plateau {
    private final int xUpperLimit;
    private final int yUpperLimit;

    Plateau(int xUpperLimit, int yUpperLimit) {
        this.xUpperLimit = xUpperLimit;
        this.yUpperLimit = yUpperLimit;
    }

    boolean isPositionExisting(int xCoordinate, int yCoordinate) {
        return yCoordinate <= yUpperLimit && yCoordinate > -1 && xCoordinate <= xUpperLimit && xCoordinate > -1;
    }
}
