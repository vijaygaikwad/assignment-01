package com.pathashala;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RoverTest {

    @Test
    void roverShouldTurnFromNorthToWestIfCommandL() {
        Rover rover = new Rover(0, 0, 'N');
        rover.turn('L');
        assertEquals('W', rover.getFace());
    }

    @Test
    void roverShouldTurnFromWestToSouthIfCommandL() {
        Rover rover = new Rover(0, 0, 'W');
        rover.turn('L');
        assertEquals('S', rover.getFace());
    }

    @Test
    void roverShouldTurnFromSouthToEastIfCommandL() {
        Rover rover = new Rover(0, 0, 'S');
        rover.turn('L');
        assertEquals('E', rover.getFace());
    }

    @Test
    void roverShouldTurnFromEastToNorthIfCommandL() {
        Rover rover = new Rover(0, 0, 'E');
        rover.turn('L');
        assertEquals('N', rover.getFace());
    }

    @Test
    void roverShouldTurnFromNorthToEastIfCommandR() {
        Rover rover = new Rover(0, 0, 'N');
        rover.turn('R');
        assertEquals('E', rover.getFace());
    }

    @Test
    void roverShouldTurnFromEastToSouthIfCommandR() {
        Rover rover = new Rover(0, 0, 'E');
        rover.turn('R');
        assertEquals('S', rover.getFace());
    }

    @Test
    void roverShouldTurnFromSouthToWestIfCommandR() {
        Rover rover = new Rover(0, 0, 'S');
        rover.turn('R');
        assertEquals('W', rover.getFace());
    }

    @Test
    void roverShouldTurnFromWestToNorthIfCommandR() {
        Rover rover = new Rover(0, 0, 'W');
        rover.turn('R');
        assertEquals('N', rover.getFace());
    }

    @Test
    void roverMovesToNorthOnCommandM() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(1, 1, 'N');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(1, 2, 'N'), roverWithNewPosition);
    }

    @Test
    void roverMovesToSouthOnCommandM() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(1, 1, 'S');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(1, 0, 'S'), roverWithNewPosition);
    }

    @Test
    void roverMovesToWestOnCommandM() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(1, 1, 'W');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(0, 1, 'W'), roverWithNewPosition);
    }

    @Test
    void roverMovesToEastOnCommandM() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(1, 1, 'E');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(2, 1, 'E'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheNorthBoundaryOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(3, 5, 'N');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(3, 5, 'N'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheSouthBoundaryOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(3, 0, 'S');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(3, 0, 'S'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheWestBoundaryOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(0, 3, 'W');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(0, 3, 'W'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheEastBoundaryOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(5, 3, 'E');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(5, 3, 'E'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheNorthEastCornerOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(5, 5, 'N');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(5, 5, 'N'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheNorthWestCornerOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(0, 5, 'W');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(0, 5, 'W'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheSouthWestCornerOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(0, 0, 'S');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(0, 0, 'S'), roverWithNewPosition);
    }

    @Test
    void roverStallsAtTheSouthEastCornerOfThePlateau() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(5, 0, 'E');
        Rover roverWithNewPosition = rover.move(plateau);
        assertEquals(new Rover(5, 0, 'E'), roverWithNewPosition);
    }

    @Test
    void roverMovesOnThePlateauGivenStringOfCommands() {
        Plateau plateau = new Plateau(5, 5);
        Rover rover = new Rover(1, 2, 'N');
        Rover otherRover = new Rover(3, 3, 'E');
        String commandsForFirstRover = "LMLMLMLMM";
        String commandsForOtherRover = "MMRMMRMRRM";
        Rover roverWithNewPosition = rover.execute(plateau, commandsForFirstRover);
        Rover otherRoverWithNewPosition = otherRover.execute(plateau, commandsForOtherRover);
        assertTrue(new Rover(1, 3, 'N').equals(roverWithNewPosition)
                && new Rover(5, 1, 'E').equals(otherRoverWithNewPosition));

    }
}
